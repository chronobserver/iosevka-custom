#!/bin/bash -e

git submodule update --remote --depth=1

pushd Iosevka >/dev/null
npm install
cp ../private-build-plans.toml .
npm run build -- ttf::iosevka-custom
popd >/dev/null

for f in Iosevka/dist/iosevka-custom/ttf/*; do
  vendor/font-patcher "$f" --mono \
    --complete --careful --makegroups -1 \
    --progressbars --glyphdir vendor/glyphs/ \
    --outputdir "$XDG_DATA_HOME/fonts"
done

fc-cache && fc-cache-32
